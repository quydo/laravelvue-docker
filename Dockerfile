FROM php:8.2-fpm-alpine3.17 as php_builder

WORKDIR /

ADD https://github.com/mlocati/docker-php-extension-installer/releases/latest/download/install-php-extensions /usr/local/bin/

RUN chmod +x /usr/local/bin/install-php-extensions

RUN install-php-extensions gd intl opcache pdo_mysql zip xdebug xsl

RUN apk add --no-cache nginx bash git acl \
  && rm -rf /var/cache/apk/*

# Set the working directory

# Set environment variable to allow running Composer as root
ENV COMPOSER_ALLOW_SUPERUSER 1

# Install Composer
COPY --from=composer:2.5.8 /usr/bin/composer /usr/bin/composer

ARG ACAN_ARG_GIT_REPO
#RUN mkdir ~/.ssh && ssh-keyscan gitlab.com >> /root/.ssh/known_hosts
# RUN mkdir -p /var/www/html

ARG CAN_VER
ARG CACHEBUST=$CAN_VER
RUN git clone ${ACAN_ARG_GIT_REPO} /var/www/html

WORKDIR /var/www/html

RUN cp .env.example .env

COPY vhost.conf /etc/nginx/http.d/vhost.conf
COPY vhost-vue.conf /etc/nginx/http.d/vhost-vue.conf

# Install Laravel dependencies
RUN composer install --no-interaction --no-dev --optimize-autoloader

# Set folder permissions for Laravel
RUN chown -R www-data:www-data storage bootstrap/cache

# Generate application key
RUN php artisan key:generate

# rewrite env from build args
ENV ACAN_APP_NAME="aCAN CMS"
ENV ACAN_DB_DRIVER="mysql"
ENV ACAN_DB_HOST="localhost"
ENV ACAN_DB_PORT="3307"
ENV ACAN_DB_DATABASE="database"
ENV ACAN_DB_USERNAME="root"
ENV ACAN_DB_PASSWORD="root"
ENV ACAN_BASE_URL="https://projectBaseUrl.com"
ENV ACAN_ENVIRONMENT=production
ENV ACAN_MAX_FILE_SIZE=64M

ENV ACAN_MAIL_MAILER=
ENV ACAN_MAIL_HOST=
ENV ACAN_MAIL_PORT=
ENV ACAN_MAIL_USERNAME=
ENV ACAN_MAIL_PASSWORD=
ENV ACAN_MAIL_ENCRYPTION=
ENV ACAN_MAIL_FROM_ADDRESS=
ENV ACAN_MAIL_FROM_NAME="${ACAN_APP_NAME}"

#RUN echo "APP_NAME=${ACAN_APP_NAME}" >> /var/www/html/.env
#RUN echo "APP_ENV=${ACAN_ENVIRONMENT}" >> /var/www/html/.env
#RUN echo "APP_URL=${ACAN_BASE_URL}" >> /var/www/html/.env
#RUN echo "MAX_FILE_SIZE=${ACAN_MAX_FILE_SIZE}" >> /var/www/html/.env
#RUN echo "DB_CONNECTION=${ACAN_DB_DRIVER}" >> /var/www/html/.env
#RUN echo "DB_HOST=${ACAN_DB_HOST}" >> /var/www/html/.env
#RUN echo "DB_PORT=${ACAN_DB_PORT}" >> /var/www/html/.env
#RUN echo "DB_DATABASE=${ACAN_DB_DATABASE}" >> /var/www/html/.env
#RUN echo "DB_USERNAME=${ACAN_DB_USERNAME}" >> /var/www/html/.env
#RUN echo "DB_PASSWORD=${ACAN_DB_PASSWORD}" >> /var/www/html/.env

FROM node:18-alpine AS build_nodejs
RUN apk add git
ARG CAN_VER_NODE
ARG ACAN_ARG_GIT_REPO
ARG CACHEBUST=$CAN_VER_NODE
RUN git clone ${ACAN_ARG_GIT_REPO} /var/www/html
WORKDIR /var/www/html/vue
RUN npm install
RUN npm run build


FROM php_builder as final_builder

COPY --from=build_nodejs /var/www/html/vue/dist ./vue/dist

COPY start.sh /start.sh

ENTRYPOINT ["sh", "/start.sh"]
